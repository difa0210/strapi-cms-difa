import React from "react";
import Head from "next/head";
import Header from "../Header";
import { withRouter } from "next/router";

const Page = ({ children }) => {
  return (
    <div className="bg-white">
      <Head>
        <title>Travel CMS</title>
        <meta name="description" content="Travel CMS" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="font-helvetica max-w-6xl px-6 mx-auto text-primary-600">
        <Header />
        <div className="mb-12">{children}</div>
      </main>
    </div>
  );
};

export default withRouter(Page);
