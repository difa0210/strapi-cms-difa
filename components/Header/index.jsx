import Link from "next/link";

const Header = () => {
  return (
    <div className="flex z-10 justify-between items-center border-b border-b-primary-500 py-2 md:py-4 mb-8">
      <Link href="/">
        <a>
          <button className="flex items-center text-xl md:text-2xl hover:(text-primary-500)">
            <p className="font-bold">CMS Admin</p>
          </button>
        </a>
      </Link>
    </div>
  );
};

export default Header;
