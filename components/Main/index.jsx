import React from "react";
import Image from "next/image";
import Link from "next/link";
import { SiYourtraveldottv } from "react-icons/si";
import { RiWalkFill } from "react-icons/ri";
import { MdChecklist } from "react-icons/md";
import { useMediaQuery } from "react-responsive";

const pages = [
  {
    name: "Travels",
    url: "/travels",
    image: <SiYourtraveldottv />,
  },
  {
    name: "Customers",
    url: "/customers",
    image: <RiWalkFill />,
  },
  {
    name: "Orders",
    url: "/orders",
    image: <MdChecklist />,
  },
];

const Main = ({ allTravel }) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 768px)" });

  return (
    <div className="grid grid-rows-1 md:grid-rows-3 gap-6">
      <div className="md:row-span-2">
        <div className="text-4xl font-semibold mb-8">
          Welcome to the Travel CMS
        </div>
        <div className="grid gap-6">
          {allTravel && (
            <Link href={`/travels/${allTravel[0]?.id}`}>
              <a className=" bg-primary-500 hover:(text-gray-100) rounded-2xl text-white w-full h-68 p-4 grid grid-rows-4">
                <div className="rounded-lg row-span-3 overflow-y-hidden mb-4">
                  <Image
                    src={allTravel[0]?.attributes.image.data.attributes.url}
                    alt="img"
                    width={isTabletOrMobile ? "360rem" : "1124rem"}
                    height="180rem"
                    objectFit="cover"
                    className="rounded-lg"
                  />
                </div>
                <div className="text-ellipsis overflow-y-scroll pr-2">
                  <p className="font-semibold text-2xl">
                    {allTravel[0]?.attributes.name}
                  </p>
                  <p className="">{allTravel[0]?.attributes.description}</p>
                </div>
              </a>
            </Link>
          )}
        </div>
      </div>
      <div className="row-span-1 grid grid-cols-1 md:grid-cols-3 gap-6">
        {pages.map((x, index) => (
          <Link key={index} href={x.url}>
            <a
              key={index}
              className="bg-gray-100 hover:(bg-gray-100) rounded-2xl text-primary-600 px-5 py-4 flex flex-col justify-end"
            >
              <div className="text-6xl">{x.image}</div>
              <div className="text-2xl font-semibold">{x.name}</div>
              <div>Create, Read, Update, Delete</div>
            </a>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Main;
