import React, { Fragment, useState } from "react";
import axios from "axios";
import { Dialog, Transition } from "@headlessui/react";
import toast from "react-hot-toast";
import { useRouter } from "next/router";

const heads = ["Id", "Name", "Email", "Phone", "Address", ""];

const Customers = ({ allCustomers }) => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [createCustomer, setCreateCustomer] = useState({
    name: "",
    phone: "",
    email: "",
    address: "",
  });
  const [updateCustomer, setUpdateCustomer] = useState({
    name: "",
    phone: "",
    email: "",
    address: "",
  });

  const handleChange = (e) => {
    setCreateCustomer({
      ...createCustomer,
      [e.target.name]: e.target.value,
    });
  };

  const handleChangeUpdate = (e) => {
    setUpdateCustomer({
      ...updateCustomer,
      [e.target.name]: e.target.value,
    });
  };

  const handleUpdate = (data) => {
    console.log(data);
    setIsUpdate(true);
    setUpdateCustomer({
      id: data.id,
      name: data.attributes.name,
      phone: data.attributes.phone,
      email: data.attributes.email,
      address: data.attributes.address,
    });
  };

  const handleSubmitUpdate = async (e) => {
    console.log(e);
    e.preventDefault();
    try {
      const response = await axios.put(
        `https://mighty-cove-68400.herokuapp.com/api/customers/${updateCustomer.id}`,
        { data: updateCustomer }
      );
      console.log(response);
      setIsUpdate(false);
      toast.success("Customer updated successfully");
      router.push("/customers");
    } catch (error) {
      console.log(error);
    }
  };

  const handleCreate = async () => {
    try {
      if (
        createCustomer.name === "" ||
        createCustomer.phone === "" ||
        createCustomer.email === "" ||
        createCustomer.address === ""
      ) {
        toast.error("Please fill all fields");
      } else {
        const response = await axios.post(
          `https://mighty-cove-68400.herokuapp.com/api/customers`,
          { data: createCustomer }
        );
        console.log(response);
        setIsOpen(false);
        toast.success("Create successfully");
        router.push("/customers");
      }
    } catch (error) {
      console.log(error);
    }
  };
  const handleDelete = async (id) => {
    try {
      const response = await axios.delete(
        `https://mighty-cove-68400.herokuapp.com/api/customers/${id}`
      );
      console.log(response);
      toast.success("Delete successfully");
      router.push("/customers");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="">
      <div className="flex items-center justify-between mb-6">
        <div className="text-4xl font-semibold ">Customers</div>
        <button
          onClick={() => setIsOpen(true)}
          className="bg-primary-500 hover:(bg-primary-400 text-gray-100) rounded-full px-4 py-1 text-white text-xl"
        >
          Create
        </button>
      </div>
      <div className="text-left">
        <div className="hidden md:(grid grid-cols-6 py-0 px-6) bg-primary-500 rounded-full text-white">
          {heads.map((x, index) => (
            <div className="py-1 font-semibold" key={index}>
              {x}
            </div>
          ))}
        </div>
        <div className="">
          {allCustomers &&
            allCustomers.map((x, index) => (
              <div
                key={index}
                className="not-last:border-b border-b-primary-200 py-4 font-semibold flex items-center md:(grid grid-cols-6 px-6)"
              >
                <div className="">{x.id}</div>
                <div className="">{x.attributes.name}</div>
                <div className=" truncate">{x.attributes.email}</div>
                <div className="">{x.attributes.phone}</div>
                <div className="truncate">{x.attributes.address}</div>
                <div className="py-2 flex items-center gap-2">
                  <button
                    onClick={() => handleUpdate(x)}
                    className="bg-primary-500 hover:(bg-primary-400 text-gray-100) rounded-full px-4 py-1 text-white "
                  >
                    Edit
                  </button>
                  <button
                    onClick={() => handleDelete(x.id)}
                    className="bg-primary-500 hover:(bg-primary-400 text-gray-100) rounded-full px-4 py-1 text-white"
                  >
                    Delete
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
      <Transition appear show={isOpen || isUpdate} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={() => setIsOpen(false) || setIsUpdate(false)}
        >
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed top-5 bg-primary-500 bg-opacity-25" />
          </Transition.Child>
          <div className="fixed inset-x-0 top-0 py-24 px-6 md:(px-0 py-32) bg-black bg-opacity-45 h-screen overflow-y-hidden">
            <div className="flex items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden border border-primary-500 rounded-3xl bg-white px-6 py-8 transition-all text-left">
                  {isOpen ? (
                    <>
                      {" "}
                      <form className="text-black mb-3">
                        <label htmlFor="">Name</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="text"
                          placeholder="Input your name"
                          value={createCustomer.name}
                          onChange={handleChange}
                          name="name"
                        />
                        <label htmlFor="">Phone</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="text"
                          placeholder="Input your phone"
                          value={createCustomer.phone}
                          onChange={handleChange}
                          name="phone"
                        />
                        <label htmlFor="">Email</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="email"
                          placeholder="Input your email"
                          value={createCustomer.email}
                          onChange={handleChange}
                          name="email"
                        />
                        <label htmlFor="">Address</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="textarea"
                          placeholder="Input your address"
                          value={createCustomer.address}
                          onChange={handleChange}
                          name="address"
                        />
                      </form>
                      <button
                        onClick={handleCreate}
                        className="rounded-lg bg-primary-500 w-full py-2 mx-auto mt-6 mb-2 text-white text-lg font-bold"
                      >
                        Create Customer
                      </button>
                    </>
                  ) : (
                    <>
                      <form className="text-black mb-3">
                        <label htmlFor="">Name</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="text"
                          placeholder="Input your name"
                          value={updateCustomer.name}
                          onChange={handleChangeUpdate}
                          name="name"
                        />
                        <label htmlFor="">Phone</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="number"
                          placeholder="Input your phone"
                          value={updateCustomer.phone}
                          onChange={handleChangeUpdate}
                          name="phone"
                        />
                        <label htmlFor="">Email</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="email"
                          placeholder="Input your email"
                          value={updateCustomer.email}
                          onChange={handleChangeUpdate}
                          name="email"
                        />
                        <label htmlFor="">Address</label>
                        <input
                          className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                          type="textarea"
                          placeholder="Input your address"
                          value={updateCustomer.address}
                          onChange={handleChangeUpdate}
                          name="address"
                        />
                      </form>
                      <button
                        onClick={handleSubmitUpdate}
                        className="rounded-lg bg-primary-500 w-full py-2 mx-auto mt-6 mb-2 text-white text-lg font-bold"
                      >
                        Update Customer
                      </button>{" "}
                    </>
                  )}
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  );
};

export default Customers;

export const getServerSideProps = async () => {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/customers?populate=deep`
  );
  return {
    props: {
      allCustomers: response.data.data,
    },
  };
};
