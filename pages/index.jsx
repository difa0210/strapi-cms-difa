import axios from "axios";
import Main from "../components/Main";

export default function Home({ allTravels }) {
  return (
    <div className="text-primary-600">
      <Main allTravel={allTravels} />
    </div>
  );
}

export const getServerSideProps = async () => {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/travel-packages?populate=deep`
  );

  return {
    props: {
      allTravels: response.data.data,
    },
  };
};
