import React, { Fragment, useEffect, useState } from "react";
("react");
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import { Dialog, Transition, Menu } from "@headlessui/react";
import toast from "react-hot-toast";
import { useRouter } from "next/router";

const Travels = ({ allTravels, allCustomers }) => {
  const router = useRouter();
  const [preview, setPreview] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenOrder, setIsOpenOrder] = useState(false);
  const [selected, setSelected] = useState(allCustomers[0]);
  const [selectedTravel, setSelectedTravel] = useState([]);
  const [total, setTotal] = useState(0);
  const [createTravel, setCreateTravel] = useState({
    name: "",
    price: "",
    description: "",
    image: "",
  });

  const handleChange = (e) => {
    setCreateTravel({
      ...createTravel,
      [e.target.name]:
        e.target.type === "file" ? e.target.files[0] : e.target.value,
    });

    e.target.type === "file" &&
      setPreview(URL.createObjectURL(e.target.files[0]));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const formData = new FormData();
      formData.append(`files`, createTravel.image, createTravel.image.name);
      const responseImage = await axios.post(
        `https://mighty-cove-68400.herokuapp.com/api/upload`,
        formData
      );
      const { data } = responseImage;

      const response = await axios.post(
        `https://mighty-cove-68400.herokuapp.com/api/travel-packages`,
        { data: { ...createTravel, image: data[0].id } }
      );
      console.log(response);
      setIsOpen(false);
      toast.success("Travel created successfully");
      router.push("/travels");
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmitOrder = async (e) => {
    e.preventDefault();
    try {
      const responseOrder = await axios.post(
        `https://mighty-cove-68400.herokuapp.com/api/orders`,
        { data: { total_price: total, customer: selected.id } }
      );
      const { data } = responseOrder;
      selectedTravel.forEach(async (travel) => {
        try {
          await axios.post(
            `https://mighty-cove-68400.herokuapp.com/api/order-details`,
            {
              data: {
                travel_package: travel.id,
                price: travel.attributes.price,
                order: data.data.id,
              },
            }
          );
        } catch (error) {
          console.log(error);
        }
      }),
        setIsOpenOrder(false);
      toast.success("Order created successfully");
      router.push("/orders");

      console.log(responseOrder);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (selectedTravel.length > 0) {
      const totalPrice = selectedTravel.reduce((acc, curr) => {
        return parseInt(acc) + parseInt(curr.attributes.price);
      }, 0);
      setTotal(totalPrice);
    } else {
      setTotal(0);
    }
  }, [selectedTravel]);

  return (
    <div className="">
      <div className="flex items-center justify-between mb-6">
        <div className="text-4xl font-semibold ">Travels</div>
        <button
          onClick={() => setIsOpen(true)}
          className="bg-primary-500 hover:(bg-primary-400 text-gray-100) rounded-full px-4 py-1 text-white text-xl"
        >
          Create
        </button>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
        {allTravels &&
          allTravels.map((x, index) => (
            <div
              key={index}
              className="bg-primary-500 rounded-2xl p-4 text-white"
            >
              <Link href={`/travels/${x.id}`} key={index}>
                <a>
                  <Image
                    src={x.attributes.image.data?.attributes.url}
                    alt="img"
                    width="360rem"
                    height="260rem"
                    objectFit="cover"
                    className="rounded-lg"
                  />
                </a>
              </Link>
              <div className="flex items-end justify-between gap-4">
                <div className="overflow-hidden">
                  <p className="text-2xl font-semibold my-2 truncate">
                    {x.attributes.name}
                  </p>
                  <p className="leading-tight truncate w-full">
                    {x.attributes.description}
                  </p>
                </div>
                <button
                  className="bg-white text-primary-500 hover:(bg-gray-100) rounded-full px-4 py-1 font-semibold"
                  onClick={() => setIsOpenOrder(true)}
                >
                  Buy
                </button>
              </div>
            </div>
          ))}
      </div>
      <Transition appear show={isOpen || isOpenOrder} as={Fragment}>
        {isOpen ? (
          <Dialog
            as="div"
            className="relative z-10"
            onClose={() => setIsOpen(false)}
          >
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="absolute w-screen h-screen top-0 bg-primary-500" />
            </Transition.Child>
            <div className="fixed inset-x-0 top-0 py-24 px-6 md:(px-0 py-32) bg-black bg-opacity-45 h-screen overflow-y-auto">
              <div className="flex items-center justify-center text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden border border-primary-500 rounded-3xl bg-white px-6 py-8 transition-all text-left flex flex-col">
                    <form className="text-black mb-3">
                      <label htmlFor="">Name</label>
                      <input
                        className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                        type="text"
                        placeholder="input travel name"
                        value={createTravel.name}
                        onChange={handleChange}
                        name="name"
                      />
                      <label htmlFor="">Price</label>
                      <input
                        className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                        type="number"
                        placeholder="input travel price"
                        value={createTravel.price}
                        onChange={handleChange}
                        name="price"
                      />
                      <label htmlFor="">Description</label>
                      <textarea
                        className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                        type="text"
                        placeholder="input travel description"
                        value={createTravel.description}
                        onChange={handleChange}
                        name="description"
                      />
                      <label htmlFor="">Image</label>
                      <input
                        className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                        type="file"
                        placeholder="input travel image"
                        onChange={handleChange}
                        name="image"
                      />
                    </form>
                    {preview && (
                      <Image
                        src={preview}
                        alt="img"
                        width="320rem"
                        height="260rem"
                        objectFit="cover"
                        className="rounded-lg"
                      />
                    )}
                    <button
                      onClick={handleSubmit}
                      className="rounded-lg bg-primary-500 hover:(bg-primary-400 text-gray-100) w-full py-2 mx-auto mt-6 mb-2 text-white text-lg font-bold"
                    >
                      Create Travel
                    </button>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        ) : (
          <Dialog
            as="div"
            className="relative z-10"
            onClose={() => setIsOpenOrder(false)}
          >
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed top-5 bg-primary-500 bg-opacity-25" />
            </Transition.Child>
            <div className="fixed inset-x-0 top-0 py-24 px-6 md:(px-0 py-32) bg-black bg-opacity-45 h-screen overflow-y-hidden">
              <div className="flex items-center justify-center text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden border border-primary-500 min-h-xl rounded-3xl bg-white text-primary-500 p-4 transition-all text-left flex flex-col items-center">
                    <div className="max-h-md w-full overflow-y-scroll px-2">
                      {allTravels &&
                        allTravels.map((x, index) => (
                          <div
                            key={index}
                            onClick={() => {
                              if (selectedTravel.some((y) => y.id === x.id)) {
                                setSelectedTravel(
                                  selectedTravel.filter((y) => y.id !== x.id)
                                );
                              } else {
                                setSelectedTravel([...selectedTravel, x]);
                              }
                            }}
                            className={`${
                              selectedTravel.some((y) => y.id === x.id)
                                ? "bg-gray-100"
                                : "bg-white"
                            } cursor-pointer border border-primary-500 rounded-2xl p-4 mb-4`}
                          >
                            <div className="flex justify-between gap-4">
                              <div className="truncate">
                                <p className="text-lg font-semibold">
                                  {x.attributes.name}
                                </p>
                                <p>
                                  {new Intl.NumberFormat("id-ID", {
                                    style: "currency",
                                    currency: "IDR",
                                  }).format(x.attributes.price)}
                                </p>
                              </div>
                              <Image
                                src={x.attributes.image.data?.attributes.url}
                                alt="img"
                                width="60rem"
                                height="60rem"
                                objectFit="cover"
                                className="rounded-lg"
                              />
                            </div>
                          </div>
                        ))}
                    </div>
                    <div className="flex w-full justify-between gap-4 my-4 font-medium px-2">
                      <div>Total Price :</div>
                      <div className="text-lg">
                        {new Intl.NumberFormat("id-ID", {
                          style: "currency",
                          currency: "IDR",
                        }).format(total)}{" "}
                      </div>
                    </div>
                    <Menu
                      as="div"
                      className="relative inline-block text-left w-full mb-6"
                    >
                      <div>
                        <Menu.Button className="inline-flex w-full justify-center rounded-full border border-primary-500 px-4 py-1 text-lg font-medium text-primary-500 hover:(bg-gray-50)">
                          {selected.attributes.name}
                        </Menu.Button>
                      </div>
                      <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        <Menu.Items
                          className="absolute bottom-14 right-0 mt-2 w-full max-h-18 overflow-y-scroll rounded-md 
                        bg-white border border-primary-500 text-primary-500 shadow-lg"
                        >
                          <div className="px-1 py-1 flex flex-col">
                            {allCustomers.map((x, index) => (
                              <Menu.Item key={index}>
                                <button
                                  onClick={() => setSelected(x)}
                                  className="text-left hover:(bg-gray-100)font-medium py-1 mx-2"
                                >
                                  {x.attributes.name}
                                </button>
                              </Menu.Item>
                            ))}
                          </div>
                        </Menu.Items>
                      </Transition>
                    </Menu>
                    <button
                      onClick={handleSubmitOrder}
                      className="bg-primary-500 hover:(bg-primary-400 text-gray-100) text-white font-semibold mx-auto px-6 py-1 rounded-full"
                    >
                      Order Now
                    </button>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        )}
      </Transition>
    </div>
  );
};

export default Travels;

export const getServerSideProps = async () => {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/travel-packages?populate=deep`
  );

  const responseCustomers = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/customers?populate=deep`
  );

  return {
    props: {
      allTravels: response.data.data,
      allCustomers: responseCustomers.data.data,
    },
  };
};
