import React, { Fragment, useState } from "react";
import Image from "next/image";
import axios from "axios";
import toast from "react-hot-toast";
import { Dialog, Transition } from "@headlessui/react";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";

const TravelDetail = ({ travel }) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 768px)" });
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [preview, setPreview] = useState(null);
  const [updateTravel, setUpdateTravel] = useState({
    name: "",
    price: "",
    description: "",
    image: "",
  });

  const handleChange = (e) => {
    setUpdateTravel({
      ...updateTravel,
      [e.target.name]:
        e.target.type === "file" ? e.target.files[0] : e.target.value,
    });

    if (e.target.type === "file") {
      let url = URL.createObjectURL(e.target.files[0]);
      setPreview(url);
    }
  };

  const handleDelete = async () => {
    try {
      const response = await axios.delete(
        `https://mighty-cove-68400.herokuapp.com/api/travel-packages/${travel.id}`
      );
      console.log(response);
      toast.success("Travel deleted successfully");
      router.push("/travels");
    } catch (error) {
      console.log(error);
    }
  };

  const handleUpdate = () => {
    setIsOpen(true);
    setUpdateTravel({
      name: travel.attributes.name,
      price: travel.attributes.price,
      description: travel.attributes.description,
      image: "",
    });
    setPreview(travel.attributes.image.data.attributes.url);
  };

  const handleSubmitUpdate = async (e) => {
    e.preventDefault();
    try {
      if (updateTravel.image) {
        const formData = new FormData();

        formData.append("files", updateTravel?.image);
        const responseImage = await axios.post(
          `https://mighty-cove-68400.herokuapp.com/api/upload`,
          formData
        );

        await axios.put(
          `https://mighty-cove-68400.herokuapp.com/api/travel-packages/${travel.id}`,
          {
            data: {
              ...updateTravel,
              image: responseImage.data[0].id,
            },
          }
        );
      } else {
        await axios.put(
          `https://mighty-cove-68400.herokuapp.com/api/travel-packages/${travel.id}`,
          {
            data: {
              ...updateTravel,
              image: travel.attributes.image.data.id,
            },
          }
        );
      }

      setIsOpen(false);
      toast.success("Update successfully");
      router.push(`/travels/${travel.id}`);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="">
      <div className="text-4xl font-semibold mb-6">Travel Detail</div>
      <div className="flex justify-center mx-auto md:w-2xl gap-6">
        {travel && (
          <div className="w-full bg-primary-500 rounded-2xl p-6 text-white">
            <div className="mb-6">
              <Image
                src={travel.attributes.image.data.attributes.url}
                alt="img"
                width={isTabletOrMobile ? "360rem" : "640rem"}
                height={isTabletOrMobile ? "180rem" : "260rem"}
                objectFit="cover"
                className="rounded-lg"
              />
              <p className="text-3xl font-semibold my-2 md:truncate">
                {travel.attributes.name}
              </p>
              <p className="leading-tight w-full">
                {travel.attributes.description}
              </p>
            </div>
            <div className="flex gap-4 ">
              <button
                onClick={handleUpdate}
                className="bg-white hover:(bg-gray-100 text-primary-400) text-primary-500 px-4 py-1 rounded-full font-semibold"
              >
                Edit
              </button>
              <button
                onClick={handleDelete}
                className="bg-white hover:(bg-gray-100 text-primary-400) text-primary-500 px-4 py-1 rounded-full font-semibold"
              >
                Delete
              </button>
            </div>
          </div>
        )}
      </div>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={() => setIsOpen(false)}
        >
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed top-5 bg-primary-500 bg-opacity-25" />
          </Transition.Child>
          <div className="fixed inset-x-0 top-0 py-24 px-6 md:(px-0 py-32) bg-black bg-opacity-45 h-screen overflow-y-auto">
            <div className="flex items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden border border-primary-500 rounded-3xl bg-white px-6 py-8 transition-all text-left flex flex-col">
                  <form className="text-black mb-3">
                    <label htmlFor="">Name</label>
                    <input
                      className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                      type="text"
                      placeholder="Input your travel name"
                      value={updateTravel.name}
                      onChange={handleChange}
                      name="name"
                    />
                    <label htmlFor="">Price</label>
                    <input
                      className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                      type="number"
                      placeholder="Input your travel price"
                      value={updateTravel.price}
                      onChange={handleChange}
                      name="price"
                    />
                    <label htmlFor="">Description</label>
                    <textarea
                      className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                      type="text"
                      placeholder="Input your travel description"
                      value={updateTravel.description}
                      onChange={handleChange}
                      name="description"
                    />
                    <label htmlFor="">Image</label>
                    <input
                      className="flex mx-auto px-2 py-2 mt-1 border bg-gray-100 items-center cursor-pointer w-full h-full outline-none placeholder-gray-400"
                      type="file"
                      placeholder="Input your travel image"
                      onChange={handleChange}
                      name="image"
                    />
                  </form>
                  {preview && (
                    <Image
                      src={preview}
                      alt="img"
                      width="320rem"
                      height="260rem"
                      objectFit="cover"
                      className="rounded-lg"
                    />
                  )}
                  <button
                    onClick={handleSubmitUpdate}
                    className="rounded-lg bg-primary-500 hover:(bg-primary-400 text-gray-100) w-full py-2 mx-auto mt-6 mb-2 text-white text-lg font-bold"
                  >
                    Update Travel
                  </button>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  );
};

export default TravelDetail;

export const getServerSideProps = async ({ params }) => {
  const { id } = params;
  const travel = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/travel-packages/${id}?populate=deep`
  );
  return {
    props: {
      travel: travel.data.data,
    },
  };
};
