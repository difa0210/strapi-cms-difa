import "../styles/globals.css";
import "windi.css";
import Page from "../components/Page";
import { Toaster } from "react-hot-toast";

function MyApp({ Component, pageProps }) {
  return (
    <Page>
      <Component {...pageProps} />
      <Toaster />
    </Page>
  );
}

export default MyApp;
