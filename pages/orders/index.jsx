import React from "react";
import axios from "axios";
import Link from "next/link";

const heads = ["Id", "TotalPrice", "Customer", "CreatedAt"];

const Orders = ({ allOrders }) => {
  return (
    <div className="">
      <div className="text-4xl font-semibold mb-6">Orders</div>
      <div className="text-left">
        <div className="rounded-full bg-primary-500 text-white grid gap-6 grid-cols-3 md:(gap-0 grid-cols-4 px-6) p-2">
          {heads.map((x, index) => (
            <div className="first:(hidden md:flex) font-semibold" key={index}>
              {x}
            </div>
          ))}
        </div>
        <div>
          {allOrders.map((x, index) => (
            <Link key={index} href={`/orders/${x.id}`}>
              <a className="not-last:border-b hover:(bg-gray-100 text-primary-400)border-b-primary-200 grid gap-6 grid-cols-3 md:(gap-0 grid-cols-4 px-6 rounded-full) p-2 font-semibold">
                <div className="hidden md:flex">#{x.id}</div>
                <div className="truncate">
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                  }).format(x.attributes.total_price)}
                </div>
                <div className="">
                  {x.attributes.customer?.data.attributes.name}
                </div>
                <div className="truncate">{x.attributes.createdAt}</div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Orders;

export const getServerSideProps = async () => {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/orders?populate=deep,2`
  );
  return {
    props: {
      allOrders: response.data.data,
    },
  };
};
