import axios from "axios";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import Image from "next/image";

const OrderDetails = ({ order }) => {
  console.log(order);
  const router = useRouter();

  const handleDelete = async () => {
    try {
      const response = await axios.delete(
        `https://mighty-cove-68400.herokuapp.com/api/orders/${order.id}`
      );
      console.log(response);
      toast.success("Order deleted successfully");
      router.push(`/orders`);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="">
      <div className="flex justify-between text-4xl font-semibold mb-6">
        <div>Order Detail</div>
        <button
          className="bg-primary-500 hover:(bg-primary-400 text-gray-100) text-lg text-white rounded-full px-6"
          onClick={handleDelete}
        >
          Delete
        </button>
      </div>
      <div className="grid grid-cols-1 md:(grid-cols-3 gap-6) p-6 bg-primary-500 rounded-2xl">
        <div className="col-span-2 text-white text-lg font-medium md:mr-8">
          <p className="flex justify-between my-2">
            Order ID : <span className="font-semibold">{order.id}</span>
          </p>
          <p className="flex justify-between my-2">
            Customer :{" "}
            <span className="font-semibold">
              {order.attributes.customer.data.attributes.name}
            </span>
          </p>
          <p className="flex justify-between my-2">
            Customer ID :{" "}
            <span className="font-semibold">
              {order.attributes.customer.data.id}
            </span>
          </p>
          <p className="flex justify-between my-2">
            Total Price :{" "}
            <span className="font-semibold">
              {new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR",
              }).format(order.attributes.total_price)}
            </span>
          </p>
          <p className="flex justify-between gap-4 my-2 truncate md:gap-0">
            Date Created :{" "}
            <span className="font-semibold">{order.attributes.createdAt}</span>
          </p>
        </div>
        <div className="col-span-1">
          {order.attributes.order_details.data.map((x, index) => (
            <div
              className="bg-white rounded-lg p-4 flex justify-center gap-2 mb-6"
              key={index}
            >
              <p className="text-primary-700 text-lg font-medium">
                #{x.attributes.travel_package.data?.id}
              </p>
              <div className="flex flex-col items-center">
                <Image
                  src={
                    x.attributes.travel_package.data?.attributes.image.data
                      .attributes.url
                  }
                  alt="img"
                  width="160rem"
                  height="160rem"
                  objectFit="cover"
                  className="rounded-lg"
                />{" "}
                <p className="text-primary-700 text-lg font-semibold truncate">
                  {x.attributes.travel_package.data?.attributes.name}
                </p>
                <p className="text-primary-700 font-medium truncate">
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                  }).format(x.attributes.travel_package.data?.attributes.price)}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;

export const getServerSideProps = async ({ params }) => {
  const response = await axios.get(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/orders/${params.id}?populate=deep,4`
  );
  return {
    props: {
      order: response.data.data,
    },
  };
};
