import { defineConfig } from "windicss/helpers";

export default defineConfig({
  extract: {
    include: ["**/*.{jsx,tsx,css}"],
    exclude: ["node_modules", ".git", ".next"],
  },
  theme: {
    extend: {
      colors: {
        primary: {
          100: "#d6f9fb",
          200: "#aef3f7",
          300: "#85edf3",
          400: "#5de7ef",
          500: "#34e1eb",
          600: "#2ab4bc",
          700: "#1f878d",
          800: "#155a5e",
          900: "#0a2d2f",
        },
      },
      fontFamily: {
        helvetica: ["Helvetica Neue", "sans-serif"],
      },
    },
  },
});
